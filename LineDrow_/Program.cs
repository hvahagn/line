﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineDraw
{
    class Program
    {
        static void Main(string[] args)
        {
            List <Line> lines= new List <Line>
            {
                new Line(),
                new Square(),
                new Triangle(),
                new Rectangle()

        };

            
           
            foreach(var line in lines)
            {
                Print(line);
            }
            
         
            Console.ReadLine();
        }

        private static void Print(Line item)
            {
                string typeName = item.GetType().Name;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($"{typeName}:\t");
                Console.ResetColor();
                item.Print(15);
                Console.WriteLine();
            }
    }
}
