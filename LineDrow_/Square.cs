﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineDraw
{
    class Square:Line
    {
        

        public override void Print(int Size)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Size of Squre side is ={Size}\n");
            Console.ResetColor();
            Draw(Size);
        }

        public override void Draw (int Size=10)
        {
            
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("*");
                Console.ResetColor();
                }
                Console.WriteLine();
            }
        }
    }
}
