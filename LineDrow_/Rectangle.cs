﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineDraw
{
    class Rectangle:Line
    {
        
        public override void Print(int Height)
        {
            Console.ForegroundColor = ConsoleColor.Red;
           
            Console.WriteLine($"The height of Rectangle is {Height} and width is {Height*2}");
            Draw(Height);
            Console.ResetColor();


        }

        public override void Draw(int Height)
        {
           Width=2*Height;

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("*");
                    Console.ResetColor();
                }
                    
                Console.WriteLine();
            }
        }
    }
}
