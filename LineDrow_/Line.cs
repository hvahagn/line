﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineDraw
{
    class Line
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Size { get; set; }
        public int Row { get; set; }
        public int L { get; set; }
        public virtual void Print(int L)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Start position of x={X} and y={Y} ");
            Console.ResetColor();
            Draw(L);
        }
     
       
        public Line()
        {
            X = 10;
            Y = 15;
            L = 17;
        }
        public virtual void Draw(int L=1)
        {
            for (int i = 0; i < L; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("*");
                Console.ResetColor();
            }

        }
    }
}
