﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineDraw
{
    class Triangle:Line
    {
      
         public override void Print(int Row)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Count of Triangle's lines are ={Row}\n");
            Console.ResetColor();
            Draw(Row);
        }
        public override void Draw(int Row)
        {
           
                for (int i = 0; i < Row; i++)
                {
                    for (int j = 0; j < Row - (i + 1); j++)
                        Console.Write(" ");
                    for (int j = 0; j < 2 * i + 1; j++)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("*");
                    Console.ResetColor();
                }
                        
                    Console.WriteLine();
                }
            
        }
    }
}
